package cn.itmtr.document.download;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.exceptions.MybatisPlusException;
import com.baomidou.mybatisplus.core.toolkit.StringPool;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.InjectionConfig;
import com.baomidou.mybatisplus.generator.config.*;
import com.baomidou.mybatisplus.generator.config.po.TableInfo;
import com.baomidou.mybatisplus.generator.config.rules.DateType;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class SqlGenerator {

	/**
	 * <p>
	 * 读取控制台内容
	 * </p>
	 */
	public static String scanner(String tip) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("请输入" + tip + "：");
		if (scanner.hasNext()) {
			String ipt = scanner.next();
			if (StrUtil.isNotEmpty(ipt)) {
				return ipt;
			}
		}
		throw new MybatisPlusException("请输入正确的" + tip + "！");
	}

	/**
	 * RUN THIS
	 */
	public static void main(String[] args) {
		// 代码生成器
		AutoGenerator mpg = new AutoGenerator();

		// 全局配置
		GlobalConfig gc = new GlobalConfig();
		String projectPath = System.getProperty("user.dir");
		gc.setOutputDir(projectPath + "/src/main/java");
		gc.setAuthor("mtr");
		gc.setOpen(false);
		gc.setBaseResultMap(true);
		// 是否覆盖文件
		gc.setFileOverride(false);
		// 只使用 java.util.date
		gc.setDateType(DateType.ONLY_DATE);
		mpg.setGlobalConfig(gc);

		// 数据源配置
		DataSourceConfig dsc = new DataSourceConfig();
		dsc.setUrl("jdbc:sqlite::resource:db/itmtr_data.db");
		dsc.setDriverName("org.sqlite.JDBC");
		dsc.setUsername("");
		dsc.setPassword("");
		mpg.setDataSource(dsc);

		String model = scanner("请输入模块名称,不需要则输入0");
		if("0".equals(model)){
			model = null;
		}
		String packageModel = StrUtil.isNotEmpty(model) ? "." + model : "";
		String mapperModel = StrUtil.isNotEmpty(model) ? model + "/" : "";

		// 包配置
		PackageConfig pc = new PackageConfig();
		pc.setParent("cn.itmtr.document.download");
		pc.setEntity("model.entity" + packageModel);
		pc.setMapper("mapper" + packageModel);
		pc.setService("service" + packageModel);
		pc.setServiceImpl("service.impl" + packageModel);
		mpg.setPackageInfo(pc);

		// 自定义配置
		InjectionConfig cfg = new InjectionConfig() {
			@Override
			public void initMap() {
				// to do nothing
			}
		};
		List<FileOutConfig> focList = new ArrayList<>();
		focList.add(new FileOutConfig("/templates/mapper.xml.ftl") {
			@Override
			public String outputFile(TableInfo tableInfo) {
				// 自定义输入文件名称
				return projectPath + "/src/main/resources/mapper/" + mapperModel
						+ tableInfo.getEntityName() + "Mapper" + StringPool.DOT_XML;
			}
		});
		cfg.setFileOutConfigList(focList);
		mpg.setCfg(cfg);
		mpg.setTemplate(new TemplateConfig().setXml(null));

		// 策略配置
		StrategyConfig strategy = new StrategyConfig();

		strategy.setNaming(NamingStrategy.underline_to_camel);
		strategy.setColumnNaming(NamingStrategy.underline_to_camel);
		strategy.setRestControllerStyle(true);
		strategy.setEntityTableFieldAnnotationEnable(true);
		strategy.setEntityLombokModel(true);
		strategy.setInclude(scanner("表名,多个使用英文逗号隔开").split(","));
		strategy.setControllerMappingHyphenStyle(false);
		mpg.setStrategy(strategy);
		// 选择 freemarker 引擎需要指定如下加，注意 pom 依赖必须有！
		mpg.setTemplateEngine(new FreemarkerTemplateEngine());
		mpg.execute();
	}

}
