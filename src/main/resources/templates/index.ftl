<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="content-type" content="text/html;charset=utf-8">
    <script src="plugins/jquery/jquery-3.5.1.min.js"></script>
    <script src="plugins/layui/layui.js"></script>
    <link rel="stylesheet" type="text/css" href="plugins/layui/css/layui.css">
</head>
<body style="background-color:#FAFAFA;">
<div class="layui-container">
    <div class="layui-row layui-col-space20" style="padding:20px 0;">
        <div class="layui-col-md4"><div class="layui-card">
                <div class="layui-card-header">创建文档</div>
                <div class="layui-card-body">
                    <div class="layui-form" lay-filter="createDocumentFormFilter">
                        <div class="layui-form-item">
                            <label class="layui-form-label">文档名称</label>
                            <div class="layui-input-inline">
                                <input type="text" name="documentName" required  lay-verify="required" lay-reqText="请输入文档名称" placeholder="请输入文档名称" autocomplete="off" class="layui-input">
                            </div>
                            <div class="layui-form-mid layui-word-aux">文档名称为最后下载文档的包名称，<b>不可重复</b>。</div>
                        </div>
                        <div class="layui-form-item">
                            <label class="layui-form-label">文档网址</label>
                            <div class="layui-input-inline">
                                <input type="text" name="url" required  lay-verify="required|url" lay-reqText="请输入文档网址" placeholder="请输入文档网址" autocomplete="off" class="layui-input">
                            </div>
                            <div class="layui-form-mid layui-word-aux">文档网址为文档下载的入口。</div>
                        </div>
                        <div class="layui-form-item">
                            <div class="layui-input-block">
                                <button class="layui-btn" lay-submit lay-filter="createDocumentSubmitFilter">创建文档</button>
                            </div>
                            <div class="layui-form-mid layui-word-aux">
                                在这里创建文档之后，在右侧点击<b>启动</b>按钮，开始后台下载文档。
                                后台下载完成后，点击<b>下载</b>按钮，下载文档。<br/>
                            </div>
                        </div>
                    </div>
                    <fieldset class="layui-elem-field">
                        <legend>注意</legend>
                        <div class="layui-field-box">
                            文档只会保存网页的<b>a</b>、<b>link</b>、<b>script</b>、<b>img</b>、<b>audio</b>、<b>video</b>标签中的数据，
                            如果是通过js导入的数据或通过ajax导入的数据则不会加载。如有需要，请手动下载补全。<br/>
                            此项目仅用于获取layUI项目文档，毕竟2021年10月13日就下线了，已经看习惯了这一版。
                        </div>
                    </fieldset>
                </div>
            </div>
        </div>
        <div class="layui-col-md8">
            <div class="layui-card">
                <div class="layui-card-header">文档列表</div>
                <div class="layui-card-body">
                    <table id="documentList" lay-filter="documentListTableFilter"></table>

                    <script type="text/html" id="documentListBarContainer">
                        {{# if(d.status == 0){ }}
                        <a class="layui-btn layui-btn-xs" lay-event="start">启动</a>
                        {{# }else if(d.status == 1){ }}
                        <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="stop">终止</a>
                        {{# }else{ }}
                        <a class="layui-btn layui-btn-xs" lay-event="restart">重启</a>
                        <a class="layui-btn layui-btn-normal layui-btn-xs" lay-event="download">下载</a>
                        {{# } }}
                    </script>
                </div>
            </div>
        </div>
    </div>

    <div class="layui-row layui-panel">
        <div class="layui-tab layui-tab-brief" lay-filter="documentFilterTabFilter">
            <ul class="layui-tab-title">
                <li class="layui-this" lay-id="create">添加过滤</li>
            </ul>
            <div class="layui-tab-content">
                <div class="layui-tab-item layui-show">
                    <div class="layui-form" lay-filter="createDocumentFilterFormFilter" style="padding:10px;">
                        <div class="layui-form-item">
                            <label class="layui-form-label">文档</label>
                            <div class="layui-input-block">
                                <select id="documentFilter_documentId" lay-filter="createDocumentFilterFormFilter_documentId"
                                        name="documentId" lay-verify="required" lay-reqText="请选择文档">
                                    <option value=""></option>
                                </select>
                                <input type="hidden" name="documentName">
                            </div>
                        </div>
                        <div class="layui-form-item">
                            <label class="layui-form-label">使用正则</label>
                            <div class="layui-input-block">
                                <input type="checkbox" name="regexFlag" lay-skin="switch" lay-text="开启|关闭">
                            </div>
                        </div>
                        <div class="layui-form-item">
                            <label class="layui-form-label">过滤类型</label>
                            <div class="layui-input-block">
                                <input type="radio" lay-filter="createDocumentFilterFormFilter_filterType" name="filterType" value="REMOVE" title="去除" checked>
                                <input type="radio" lay-filter="createDocumentFilterFormFilter_filterType" name="filterType" value="REPLACE" title="替换">
                            </div>
                        </div>
                        <div class="layui-form-item layui-form-text">
                            <label class="layui-form-label">源内容</label>
                            <div class="layui-input-block">
                                <textarea name="sourceContent" lay-verify="required" lay-reqText="请输入源内容" placeholder="请输入删除或被替换的内容" class="layui-textarea"></textarea>
                            </div>
                        </div>
                        <div class="layui-form-item layui-form-text" id="targetContentFormItem" style="display:none;">
                            <label class="layui-form-label">目标内容</label>
                            <div class="layui-input-block">
                                <textarea name="targetContent" placeholder="请输入要替换的内容" class="layui-textarea"></textarea>
                            </div>
                        </div>
                        <div class="layui-form-item">
                            <div class="layui-input-block">
                                <button class="layui-btn" lay-submit lay-filter="createDocumentFilterSubmitFilter">添加过滤</button>
                            </div>
                            <div class="layui-form-mid layui-word-aux">
                                过滤内容必须在<b>启动</b>之前添加才有用
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/html" id="documentFilterBarContainer">
            <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删除</a>
        </script>
    </div>

</div>
</body>
<script>
    $(function () {
        var form = layui.form, table = layui.table, element = layui.element;
        // 创建文档
        form.on('submit(createDocumentSubmitFilter)', function(data){
            var params = data.field;
            _baseAjaxPost("/createDocument", params, function (data) {
                form.val('createDocumentFormFilter', {
                    documentName: '',
                    url: '',
                });
                table.reload('documentList');
                addDocumentFilterTab(data.data);
            });
            return false; //阻止表单跳转。如果需要表单跳转，去掉这段即可。
        });
        table.render({
            elem: '#documentList',
            url: '/documentList',
            parseData: function(res){ //res 即为原始返回的数据
                if (!res.data) {
                    res.data = {};
                }
                return {
                    "code": res.code, //解析接口状态
                    "msg": res.msg, //解析提示文本
                    "data": res.data.documentList //解析数据列表
                };
            },
            toolbar: true,
            defaultToolbar: ['filter', 'exports', {
                title: '刷新当前页', layEvent: 'refreshCurrentPage', icon: 'layui-icon-refresh'
            }],
            title: '文档列表',
            height: '500',
            page: false,
            cols: [[
                {field: 'id', title: 'ID', hide: true},
                {field: 'documentName', minWidth: 100, title: '文档名称'},
                {field: 'url', minWidth: 200, title: '网址'},
                {field: 'status', title: '状态', width: 100,
                    templet: function(res){
                        switch (res.status) {
                            case 0: return "待开始";
                            case 1: return "正在执行";
                            case 2: return "执行结束";
                            case 3: return "手动终止";
                            default: return "";
                        }
                    }
                },
                {field: 'gmtCreate', title: '创建时间', width: 166},
                {fixed: 'right', title:'操作', toolbar: '#documentListBarContainer', minWidth:100}
            ]],
            done: function(res, curr, count){
                $("#documentFilter_documentId").html('<option value=""></option>');
                res.data.forEach(function (document) {
                    $("#documentFilter_documentId").append('<option value="' + document.id + '">' + document.documentName + '</option>');
                });
                form.render('select', 'createDocumentFilterFormFilter');
            }
        });
        //头工具栏事件
        table.on('toolbar(documentListTableFilter)', function (obj) {
            switch (obj.event) {
                case 'refreshCurrentPage':
                    table.reload('documentList');
                    break;
            }
        });
        //监听行工具事件
        table.on('tool(documentListTableFilter)', function (obj) {
            var data = obj.data;
            switch (obj.event) {
                case 'start':
                    layer.confirm('确定启动下载吗？', function (index) {
                        layer.close(index);
                        _baseAjaxPost("/startDownload", {
                            documentId: data.id
                        }, function (data) {
                            layer.msg("正在下载中...");
                            table.reload('documentList');
                        });
                    });
                    break;
                case 'restart':
                    layer.confirm('确定重新启动下载吗？', function (index) {
                        layer.close(index);
                        _baseAjaxPost("/restartDownload", {
                            documentId: data.id
                        }, function (data) {
                            layer.msg("正在下载中...");
                            table.reload('documentList');
                        });
                    });
                    break;
                case 'stop':
                    layer.confirm('确定终止下载吗？', function (index) {
                        layer.close(index);
                        _baseAjaxPost("/stopDownload", {
                            documentId: data.id
                        }, function (data) {
                            layer.msg("下载已终止");
                            table.reload('documentList');
                        });
                    });
                    break;
                case 'download':
                    window.open("/downloadDocument?documentId=" + data.id);
                    break;
            }
        });

        // 添加过滤
        form.on('submit(createDocumentFilterSubmitFilter)', function(data){
            var params = data.field;
            params.regexFlag = !!params.regexFlag;
            _baseAjaxPost("/filter/create", params, function (data) {
                form.val('createDocumentFilterFormFilter', {
                    sourceContent: '',
                    targetContent: '',
                });
                table.reload('documentFilterListTable-' + params.documentId);
            });
            return false;
        });
        form.on('radio(createDocumentFilterFormFilter_filterType)', function(data){
            if (data.value === "REMOVE") {
                $("#targetContentFormItem").hide();
            } else {
                $("#targetContentFormItem").show();
            }
        });
        form.on('select(createDocumentFilterFormFilter_documentId)', function(data){
            var selectOption = $(data.elem).find('option[value="' + data.value + '"]');
            form.val("createDocumentFilterFormFilter", {
                documentName: selectOption.text()
            });
        });

        element.on('tab(documentFilterTabFilter)', function(data){
            if (data.index !== 0) {
                var layId = this.getAttribute('lay-id');
                table.reload(layId);
            }
        });

        function addDocumentFilterTab(document) {
            var id = 'documentFilterListTable-' + document.id;
            var filter = id + 'Filter';
            element.tabAdd("documentFilterTabFilter", {
                title: document.documentName,
                content: '<table id="' + id + '" lay-filter="' + filter + '"></table>',
                id: id
            });

            table.render({
                elem: '#' + id,
                url: '/filter/listByDocumentId',
                where: {
                    documentId: document.id
                },
                parseData: function(res){ //res 即为原始返回的数据
                    if (!res.data) {
                        res.data = {};
                    }
                    return {
                        "code": res.code, //解析接口状态
                        "msg": res.msg, //解析提示文本
                        "data": res.data.filterList //解析数据列表
                    };
                },
                toolbar: true,
                defaultToolbar: ['filter', 'exports', {
                    title: '刷新当前页', layEvent: 'refreshCurrentPage', icon: 'layui-icon-refresh'
                }],
                title: '文档过滤列表',
                height: '400',
                page: false,
                escape: true,
                cols: [[
                    {field: 'id', title: 'ID', hide: true},
                    {field: 'documentName', minWidth: 100, title: '文档名称'},
                    {field: 'filterType', title: '过滤类型', width: 100,
                        templet: function(res){
                            switch (res.filterType) {
                                case 'REMOVE': return "去除";
                                case 'REPLACE': return "替换";
                                default: return "";
                            }
                        }
                    },
                    {field: 'sourceContent', title: '源内容', minWidth: 150},
                    {field: 'targetContent', title: '目标内容', minWidth: 150},
                    {field: 'regexFlag', title: '使用正则', width: 100,
                        templet: function(res){
                            switch (res.regexFlag) {
                                case 0: return "不使用";
                                case 1: return "使用";
                                default: return "";
                            }
                        }
                    },
                    {field: 'gmtCreate', title: '创建时间', width: 166},
                    {fixed: 'right', title:'操作', toolbar: '#documentFilterBarContainer', width:80}
                ]]
            });
            //头工具栏事件
            table.on('toolbar(' + filter + ')', function (obj) {
                switch (obj.event) {
                    case 'refreshCurrentPage':
                        table.reload(id);
                        break;
                }
            });
            //监听行工具事件
            table.on('tool(' + filter + ')', function (obj) {
                var data = obj.data;
                switch (obj.event) {
                    case 'del':
                        layer.confirm('确定删除吗？', function (index) {
                            layer.close(index);
                            _baseAjaxPost("/filter/remove", {
                                filterId: data.id
                            }, function (data) {
                                layer.msg("过滤内容已删除");
                                table.reload(id);
                            });
                        });
                        break;
                }
            });
        }

        // 初始化列表
        _baseAjaxGet("/documentList", {}, function (data) {
            data.documentList.forEach(addDocumentFilterTab);
        });
    });
</script>
<script>
    function _baseAjax(requestPath, requestData, requestType, callback) {
        var load = layer.load();
        $.ajax({
            url: requestPath,
            type: requestType || 'GET',
            data: requestData || {},
            success: function (res) {
                layer.close(load);
                if (res.code === 0) {
                    if (callback) {
                        callback(res.data, res);
                    }
                } else {
                    layer.msg(res.msg, {anim: 6, icon: 5});
                }
            },
            error: function () {
                layer.close(load);
                layer.alert("系统异常", {anim: 6, icon: 5, title: '提示'});
            }
        });
    }

    function _baseAjaxGet(requestPath, requestData, callback) {
        _baseAjax(requestPath, requestData, "GET", callback);
    }

    function _baseAjaxPost(requestPath, requestData, callback) {
        _baseAjax(requestPath, requestData, "POST", callback);
    }

</script>
</html>