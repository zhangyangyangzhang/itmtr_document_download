package cn.itmtr.document.download.mapper;

import cn.itmtr.document.download.model.entity.DocumentFilter;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author mtr
 * @since 2021-10-06
 */
public interface DocumentFilterMapper extends BaseMapper<DocumentFilter> {

}
