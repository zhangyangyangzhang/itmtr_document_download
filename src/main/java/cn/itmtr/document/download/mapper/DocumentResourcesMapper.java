package cn.itmtr.document.download.mapper;

import cn.itmtr.document.download.model.entity.DocumentResources;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author mtr
 * @since 2021-10-04
 */
public interface DocumentResourcesMapper extends BaseMapper<DocumentResources> {

}
