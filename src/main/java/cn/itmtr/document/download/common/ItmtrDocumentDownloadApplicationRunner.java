package cn.itmtr.document.download.common;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.util.concurrent.Executor;

/**
 * 启动加载类型
 *
 * @author mtr
 * @date 2021/2/25
 */
@Slf4j
@Component
public class ItmtrDocumentDownloadApplicationRunner implements ApplicationRunner {

    @Autowired
    @Qualifier("customThreadExecutor")
    private Executor executor;

    /**
     * Spring容器启动完成后执行
     *
     * @param applicationArguments Spring容器运行参数
     * @author mtr
     * @date 2021/2/25
     */
    @Override
    public void run(ApplicationArguments applicationArguments) {
        log.info("入口链接：http://127.0.0.1:8080");
    }

}
