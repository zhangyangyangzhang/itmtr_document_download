package cn.itmtr.document.download.common.util;

import lombok.Data;

/**
 * 链接属性
 *
 * @author mtr
 * @since 2021-10-04
 */
@Data
public class UrlProperty {

    /**
     * 原链接地址通过md5的值
     */
    private String sourceUrlKey;

    /**
     * 原链接地址
     */
    private String sourceUrl;

    /**
     * 可访问链接地址
     */
    private String targetUrl;

    /**
     * 协议 http 或 https
     */
    private String protocol;

    /**
     * 域名
     */
    private String domain;

    /**
     * 地址
     */
    private String uri;

    /**
     * 参数
     */
    private String query;

    /**
     * 类型 html js css image video audio
     */
    private String type;

    /**
     * 页面路径
     */
    private String pagePath;

}
