package cn.itmtr.document.download.common.validator;


import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.NumberUtil;
import cn.hutool.core.util.ReUtil;
import cn.hutool.core.util.StrUtil;
import cn.itmtr.document.download.common.exception.ItmtrException;

import java.util.Collection;

/**
 * 断言工具类
 *
 * @author mtr
 * @since 2021-08-10
 */
public class Assert {

    /**
     * 校验字符串是否为空白
     *
     * @param str     字符串内容
     * @param message 异常提示消息
     * @author mtr
     * @date 2021/8/10
     */
    public static void isBlank(String str, String message) {
        if (StrUtil.isBlank(str)) {
            throw new ItmtrException(message);
        }
    }

    /**
     * 校验集合是否为null或为空
     *
     * @param collection 要校验的集合对象
     * @param message    异常提示消息
     * @author mtr
     * @date 2021/8/10
     */
    public static void isEmpty(Collection<?> collection, String message) {
        if (CollUtil.isEmpty(collection)) {
            throw new ItmtrException(message);
        }
    }

    /**
     * 校验对象是否为null
     *
     * @param obj     要校验的对象
     * @param message 异常提示消息
     * @author mtr
     * @date 2021/8/10
     */
    public static void isNull(Object obj, String message) {
        if (obj == null) {
            throw new ItmtrException(message);
        }
    }

    /**
     * 校验对象是否不为null
     *
     * @param obj     要校验的对象
     * @param message 异常提示消息
     * @author mtr
     * @date 2021/8/10
     */
    public static void isNotNull(Object obj, String message) {
        if (obj != null) {
            throw new ItmtrException(message);
        }
    }

    /**
     * 如果为 expression 为 true，抛出自定义异常
     *
     * @param expression 条件
     * @param message 错误消息
     * @author mtr
     * @date 2021/8/10
     */
    public static void isTrue(Boolean expression, String message) {
        if (expression) {
            throw new ItmtrException(message);
        }
    }

    /**
     * 校验字符串是否符合正则表达式内容
     * 不符合则抛出异常
     *
     * @param regex   正则
     * @param content 内容
     * @param message 异常提示消息
     * @author mtr
     * @date 2021/8/10
     */
    public static void isNotMatch(String regex, CharSequence content, String message) {
        if (!ReUtil.isMatch(regex, content)) {
            throw new ItmtrException(message);
        }
    }

    /**
     * 校验字符串是否不是数字
     *
     * @param content 内容
     * @param message 异常提示消息
     * @author mtr
     * @date 2021/8/10
     */
    public static void isNotInteger(String content, String message) {
        if (!NumberUtil.isInteger(content)) {
            throw new ItmtrException(message);
        }
    }

}
