package cn.itmtr.document.download.common.exception;

import lombok.extern.slf4j.Slf4j;

/**
 * 自定义异常
 *
 * @author mtr
 * @since 2021-08-08
 */
@Slf4j
public class ItmtrException extends RuntimeException {

    /**
     * 错误码
     */
    private Integer code;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public ItmtrException(String message) {
        this(ItmtrExceptionEnum.ERROR.getCode(), message);
    }

    public ItmtrException(ItmtrExceptionEnum exceptionEnum) {
        this(exceptionEnum.getCode(), exceptionEnum.getMessage());
    }

    public ItmtrException(Integer code, String message) {
        super(message);
        this.code = code;
    }

    public ItmtrException(Throwable e, String message) {
        super(message + "\n" + e.getMessage(), e);
        this.code = ItmtrExceptionEnum.ERROR.getCode();
    }

}
