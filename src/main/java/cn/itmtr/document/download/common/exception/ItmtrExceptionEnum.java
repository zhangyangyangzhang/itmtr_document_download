package cn.itmtr.document.download.common.exception;

/**
 * 自定义异常枚举
 *
 * @author mtr
 * @since 2021-08-08
 */
public enum ItmtrExceptionEnum {

    /**
     * 成功请求，未出错
     */
    SUCCESS(0, "success"),
    /**
     * 默认错误请求
     */
    ERROR(1, "fail"),
    /**
     * 参数有误
     */
    ERROR_PARAM(100, "参数有误"),
    /**
     * 参数有误
     */
    ERROR_PARAM_LOGIN(101, "账号或密码有误"),
    /**
     * 数据已消失或已删除
     */
    ERROR_DATA_VANISH(200, "数据不存在或已删除"),
    /**
     * 数据已存在
     */
    ERROR_DATA_EXIST(201, "数据已存在"),
    /**
     * 请先登录
     */
    NO_AUTH(401, "token无效或已过期"),
    /**
     * 暂无权限访问
     */
    NO_LOGIN(403, "请先登录"),
    ;

    /**
     * 错误码
     */
    private final Integer code;
    /**
     * 消息内容
     */
    private final String message;

    public Integer getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    ItmtrExceptionEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

}
