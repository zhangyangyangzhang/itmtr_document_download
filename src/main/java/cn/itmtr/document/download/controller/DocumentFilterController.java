package cn.itmtr.document.download.controller;


import cn.itmtr.document.download.common.util.Result;
import cn.itmtr.document.download.common.validator.Assert;
import cn.itmtr.document.download.common.validator.ValidatorUtil;
import cn.itmtr.document.download.model.dto.DocumentFilterDTO;
import cn.itmtr.document.download.model.entity.DocumentFilter;
import cn.itmtr.document.download.service.IDocumentFilterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 * 文档过滤前端控制器
 * </p>
 *
 * @author mtr
 * @since 2021-10-06
 */
@RestController
@RequestMapping("/filter")
public class DocumentFilterController {

    @Autowired
    private IDocumentFilterService documentFilterService;

    /**
     * 通过文档获取过滤列表
     *
     * @param documentId 文档id
     * @return cn.itmtr.document.download.common.util.Result
     * @author mtr
     * @date 2021/10/6
     */
    @GetMapping("/listByDocumentId")
    public Result listByDocumentId(Integer documentId) {
        List<DocumentFilter> filterList = documentFilterService.listByDocumentId(documentId);
        return Result.ok("filterList", filterList);
    }

    /**
     * 创建过滤内容
     *
     * @param filterDTO 过滤数据
     * @return cn.itmtr.document.download.common.util.Result
     * @author mtr
     * @date 2021/10/6
     */
    @PostMapping("/create")
    public Result create(DocumentFilterDTO filterDTO) {
        ValidatorUtil.validateObj(filterDTO);
        documentFilterService.create(filterDTO);
        return Result.ok();
    }

    /**
     * 删除过滤内容
     *
     * @param filterId 过滤id
     * @return cn.itmtr.document.download.common.util.Result
     * @author mtr
     * @date 2021/10/6
     */
    @PostMapping("/remove")
    public Result remove(Integer filterId) {
        Assert.isNull(filterId, "请选择删除的过滤内容");
        documentFilterService.removeById(filterId);
        return Result.ok();
    }

}
