package cn.itmtr.document.download.model.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author mtr
 * @since 2021-10-04
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("document")
public class Document implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @TableField("document_name")
    private String documentName;

    @TableField("url")
    private String url;

    @TableField("protocol")
    private String protocol;

    @TableField("domain")
    private String domain;

    @TableField("root_path")
    private String rootPath;

    /**
     * 下载状态 0待开始 1正在执行 2执行结束 3手动终止
     */
    @TableField("status")
    private Integer status;

    @TableField("gmt_create")
    private String gmtCreate;

    @TableField("gmt_update")
    private String gmtUpdate;


}
