package cn.itmtr.document.download.model.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author mtr
 * @since 2021-10-04
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("document_resources")
public class DocumentResources implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @TableField("document_id")
    private Integer documentId;

    @TableField("source_url_key")
    private String sourceUrlKey;

    @TableField("source_url")
    private String sourceUrl;

    @TableField("target_url")
    private String targetUrl;

    @TableField("protocol")
    private String protocol;

    @TableField("domain")
    private String domain;

    @TableField("uri")
    private String uri;

    @TableField("query")
    private String query;

    @TableField("html_content")
    private String htmlContent;

    @TableField("html_template")
    private String htmlTemplate;

    @TableField("type")
    private String type;

    @TableField("path")
    private String path;

    @TableField("page_path")
    private String pagePath;

    /**
     * 下载状态 0等下载 1下载成功 2下载失败
     */
    @TableField("download_status")
    private Integer downloadStatus;

    @TableField("gmt_create")
    private String gmtCreate;

    @TableField("gmt_update")
    private String gmtUpdate;


}
