package cn.itmtr.document.download.service;

import cn.itmtr.document.download.model.entity.Document;
import cn.itmtr.document.download.model.entity.DocumentResources;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author mtr
 * @since 2021-10-04
 */
public interface IDocumentResourcesService extends IService<DocumentResources> {

    /**
     * 开始下载资源
     *
     * @param document 文档数据
     * @author mtr
     * @date 2021/10/4
     */
    void downloadResources(Document document);

    DocumentResources getBySourceUrlKey(Integer documentId, String sourceUrlKey);

    /**
     * 通过文档id获取所有资源map
     * key：source_url_key
     * value：page_path
     *
     * @param documentId 文档id
     * @return java.util.Map<java.lang.String, java.lang.String>
     * @author mtr
     * @date 2021/10/5
     */
    Map<String, String> getResourcesMap(Integer documentId);

    /**
     * 通过类型获取列表
     *
     * @param documentId 文档id
     * @param type       类型
     * @return java.util.List<cn.itmtr.document.download.model.entity.DocumentResources>
     * @author mtr
     * @date 2021/10/5
     */
    List<DocumentResources> listByType(Integer documentId, String type);

    /**
     * 通过文档id删除
     *
     * @param documentId 文档id
     * @author mtr
     * @date 2021/10/18
     */
    void removeByDocumentId(Integer documentId);

}
