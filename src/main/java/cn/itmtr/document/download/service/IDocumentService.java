package cn.itmtr.document.download.service;

import cn.itmtr.document.download.model.entity.Document;
import com.baomidou.mybatisplus.extension.service.IService;

import javax.servlet.http.HttpServletResponse;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author mtr
 * @since 2021-10-04
 */
public interface IDocumentService extends IService<Document> {

    /**
     * 创建文档
     *
     * @param documentName 文档名称
     * @param url          文档链接
     * @return cn.itmtr.document.download.model.entity.Document
     * @author mtr
     * @date 2021/10/6
     */
    Document createDocument(String documentName, String url);

    /**
     * 更新文档状态
     *
     * @param id     id
     * @param status 状态
     * @author mtr
     * @date 2021/10/4
     */
    void updateStatus(Integer id, Integer status);

    /**
     * 下载文档
     *
     * @param document  文档数据
     * @param response  response
     * @param userAgent UA
     * @author mtr
     * @date 2021/10/5
     */
    void downloadDocument(Document document, HttpServletResponse response, String userAgent);

    /**
     * 删除文档已经下载的内容
     *
     * @param document 文档数据
     * @author mtr
     * @date 2021/10/18
     */
    void removeFile(Document document);

}
