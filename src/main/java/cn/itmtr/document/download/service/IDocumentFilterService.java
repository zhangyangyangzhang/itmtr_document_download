package cn.itmtr.document.download.service;

import cn.itmtr.document.download.model.dto.DocumentFilterDTO;
import cn.itmtr.document.download.model.entity.DocumentFilter;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author mtr
 * @since 2021-10-06
 */
public interface IDocumentFilterService extends IService<DocumentFilter> {

    /**
     * 通过文档id获取列表
     *
     * @param documentId 文档id
     * @return java.util.List<cn.itmtr.document.download.model.entity.DocumentFilter>
     * @author mtr
     * @date 2021/10/6
     */
    List<DocumentFilter> listByDocumentId(Integer documentId);

    /**
     * 创建过滤内容
     *
     * @param filterDTO 过滤内容
     * @author mtr
     * @date 2021/10/6
     */
    void create(DocumentFilterDTO filterDTO);

    /**
     * 过滤文档html内容
     *
     * @param documentFilters 文档过滤内容
     * @param html            html内容
     * @return java.lang.String
     * @author mtr
     * @date 2021/10/9
     */
    String filterDocumentHtml(List<DocumentFilter> documentFilters, String html);

}
