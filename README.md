
## 为什么做这个

搞这个事的主要原因是`layui`重要公告，内容如下：

![image.png](https://file.itmtr.cn/itmtr/image_1632638135161.png)

从18年入行就接触到了layer，就觉得巨好用，就一直在用，也在推荐身边的人用！

直到后来使用layui，作为一个后端程序员，简直太好用了。

现在突然说官网要下架了，还是很难过的。

我会一直支持layui的。

不过这版文档已经用习惯了，新版虽然还有文档，但我怕用不习惯，所以想把这个文档保存一份静态的。但人工保存工作量太大所有便有了此想法。

我自己下载的文档网站：[http://layui.itmtr.cn/](http://layui.itmtr.cn/)

我自己下载的文档网站项目：[https://gitee.com/itmtr/layui_doc](https://gitee.com/itmtr/layui_doc)

## 项目介绍

用于下载文档站，输入文档网页首页入口，设置过滤条件，启动下载，过一会刷新查看是否下载完成，下载完成后点击下载即可。

![文档下载项目首页](https://images.gitee.com/uploads/images/2021/1012/152217_5a532978_8718280.png "文档下载项目首页.png")

![文档](https://images.gitee.com/uploads/images/2021/1012/152306_2ef3a1c2_8718280.png "文档.png")

![添加过滤](https://images.gitee.com/uploads/images/2021/1012/152322_a337d617_8718280.png "添加过滤.png")

![过滤列表](https://images.gitee.com/uploads/images/2021/1012/152335_e57986c8_8718280.png "过滤列表.png")

## 项目使用

下载jar包或clone本项目打成jar包。
启动jar包，浏览器输入网址`127.0.0.1:8080`进行操作即可。

## 包结构

```
cn.itmtr.document.download
├── common            // 通用代码
│       └── constant                                // 通用常量
│       └── exception                               // 异常处理
│       └── util                                    // 工具包
│       └── DocumentDownloadApplicationRunner       // 项目启动运行
├── config            // 全局配置
├── controller        // 控制器
├── mapper            // mapper
├── model             // 对象实体
│       └── dto                                      // 数据传输对象
│       └── entity                                   // 表实体
│       └── enums                                    // 枚举
│       └── vo                                       // web展示对象
├── service           // 服务类
│       └── impl                                     // 服务实现类
├── queue             // 队列
├── runnable          // 线程类
├── task              // 定时任务
```

## 注意

1. 此项目仅用于保存`layui`等文档网站，不可用于其他用途。
2. 此项目只能保存网页静态资源，通过`js`等`ajax`加载的数据无法保存。
3. 有任何建议或BUG可以提issues，我会抽时间解决的。

